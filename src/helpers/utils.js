/*****************************
 *  Вспомогательные функции  *
 *****************************/

/**
 * Возвращает случайное целое число между min (включительно) и max (не включая max)
 * @param min минимум
 * @param max максимум
 * @return {Number}
 */
export function getRandomIntInRange(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

/**
 * Преобразует секунды в минуты и секунды
 * @param number секунды
 * @returns {number[]} массив вида [минуты, секунды]
 */
export function secondsToMinutesSeconds(number) {
  if (typeof number === 'string') number = Number(number);
  return [Math.floor(number / 60), number % 60]
}

/**
 * Возвращает подходящее название для указанного времени
 * @param number количество секунд/минут/...
 * @param array массив строк, типа ['минута', 'минуты', 'минут'] или ['minute', 'minutes']
 * @param lang язык ru/en
 * @returns string
 */
export function timeToString(number, array, lang) {
  if (typeof number === 'string') number = Number(number);
  if (typeof array === 'string') return array;
  if (lang === 'ru') {
    let tens = number % 100;
    let ones = number % 10;
    if (tens > 10 && tens < 20) return array[2];
    if (ones > 1 && ones < 5) return array[1];
    if (ones === 1) return array[0];
    return array[2];
  } else if (lang === 'en') {
    if (number === 1) return array[1];
    else return array[0];
  }
}

/**
 * Является ли строка валидным URL-адресом
 * @param {string} url
 * @returns {boolean}
 */
export function isValidURL(url) {
  const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  
  return !!pattern.test(url);
}

/**
 * Составить строку параметров из объекта
 * @param {object} source
 * @return {string}
 */
export function queryParams(source) {
  return Object.entries(source)
    .map(pair => `${encodeURIComponent(pair[0])}=${encodeURIComponent(pair[1])}`)
    .join('&');
}

/**
 * Округляет число
 * @param {number} num число для округления
 * @param {number} places кол-во знаков после запятой (default: 2)
 * @return {number}
 */
export function roundNumber(num, places = 2) {
  return +(Math.round(num + "e+" + places) + "e-" + places);
}
