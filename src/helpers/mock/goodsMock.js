/**
 * Моки товаров для каталога
 */

export const catalogMock = [
  {
    GUID: '110',
    Name: 'Молочко козье',
    Code: '211111111',
    VendorCode: '2111111111',
    Quantity: 3,
    GroupGUID: '21111111',
    Prices: [
      {
        Price: 100,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '111',
    Name: 'Хлебушек цельнозерновой',
    Code: '211111112',
    VendorCode: '2111111112',
    Quantity: 10,
    GroupGUID: '2111112',
    Prices: [
      {
        Price: 50,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '112',
    Name: 'Мороженка щербетик',
    Code: '211111113',
    VendorCode: '2111111113',
    Quantity: 1,
    GroupGUID: '21111113',
    Prices: [
      {
        Price: 150.2,
        Type: 'default'
      },
      {
        Price: 50.6,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '113',
    Name: 'Кашка манная',
    Code: '211111114',
    VendorCode: '2111111114',
    Quantity: 0,
    GroupGUID: '21111114',
    Prices: [
      {
        Price: 10,
        Type: 'default'
      },
      {
        Price: 0,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '114',
    Name: 'Помидорчики сочненькие',
    Code: '211111115',
    VendorCode: '2111111115',
    Quantity: 4,
    GroupGUID: '21111115',
    Prices: [
      {
        Price: 15,
        Type: 'default'
      },
      {
        Price: 5,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '115',
    Name: 'Творожок жирненький',
    Code: '211111116',
    VendorCode: '2111111116',
    Quantity: 7,
    GroupGUID: '21111116',
    Prices: [
      {
        Price: 20.3,
        Type: 'default'
      },
      {
        Price: 5.5,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '116',
    Name: 'Огурчики с грядочки',
    Code: '211111117',
    VendorCode: '2111111117',
    Quantity: 5,
    GroupGUID: '21111117',
    Prices: [
      {
        Price: 20,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '117',
    Name: 'Чипсики хрустященькие',
    Code: '211111118',
    VendorCode: '2111111118',
    Quantity: 12,
    GroupGUID: '21111118',
    Prices: [
      {
        Price: 30,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '118',
    Name: 'Клубничка со сливочками',
    Code: '211111119',
    VendorCode: '2111111119',
    Quantity: 7,
    GroupGUID: '21111119',
    Prices: [
      {
        Price: 20,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '119',
    Name: 'Арбузик сезонный',
    Code: '211111120',
    VendorCode: '2111111120',
    Quantity: 3,
    GroupGUID: '21111120',
    Prices: [
      {
        Price: 100,
        Type: 'default'
      },
      {
        Price: 50,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '120',
    Name: 'Молочко коровье',
    Code: '111111111',
    VendorCode: '1111111111',
    Quantity: 3,
    GroupGUID: '11111111',
    Prices: [
      {
        Price: 100,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '121',
    Name: 'Хлебушек тепленький мягенький',
    Code: '111111112',
    VendorCode: '1111111112',
    Quantity: 10,
    GroupGUID: '11111112',
    Prices: [
      {
        Price: 50,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '122',
    Name: 'Мороженка холодненькая',
    Code: '111111113',
    VendorCode: '1111111113',
    Quantity: 1,
    GroupGUID: '11111113',
    Prices: [
      {
        Price: 150.2,
        Type: 'default'
      },
      {
        Price: 50.6,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '123',
    Name: 'Кашка геркулесовая',
    Code: '111111114',
    VendorCode: '1111111114',
    Quantity: 0,
    GroupGUID: '11111114',
    Prices: [
      {
        Price: 10,
        Type: 'default'
      },
      {
        Price: 0,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '124',
    Name: 'Помидорчики сладенькие',
    Code: '111111115',
    VendorCode: '1111111115',
    Quantity: 4,
    GroupGUID: '11111115',
    Prices: [
      {
        Price: 15,
        Type: 'default'
      },
      {
        Price: 5,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '125',
    Name: 'Творожок рассыпчатый',
    Code: '111111116',
    VendorCode: '1111111116',
    Quantity: 7,
    GroupGUID: '11111116',
    Prices: [
      {
        Price: 20.3,
        Type: 'default'
      },
      {
        Price: 5.5,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '126',
    Name: 'Огурчики хрустящие',
    Code: '111111117',
    VendorCode: '1111111117',
    Quantity: 5,
    GroupGUID: '11111117',
    Prices: [
      {
        Price: 20,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '127',
    Name: 'Чипсики солененькие',
    Code: '111111118',
    VendorCode: '1111111118',
    Quantity: 12,
    GroupGUID: '11111118',
    Prices: [
      {
        Price: 30,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '128',
    Name: 'Клубничка сочная',
    Code: '111111119',
    VendorCode: '1111111119',
    Quantity: 7,
    GroupGUID: '11111119',
    Prices: [
      {
        Price: 20,
        Type: 'default'
      },
      {
        Price: 10,
        Type: 'discount'
      },
    ]
  },
  {
    GUID: '129',
    Name: 'Арбузик сахарный',
    Code: '111111120',
    VendorCode: '1111111120',
    Quantity: 3,
    GroupGUID: '11111120',
    Prices: [
      {
        Price: 100,
        Type: 'default'
      },
      {
        Price: 50,
        Type: 'discount'
      },
    ]
  }
]
