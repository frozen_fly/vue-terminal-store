/**
 * @param {string} type
 * @returns {function(...*): void}
 */
const makeLogger = type => {
  let prefix;
  
  switch (type) {
    case 'log':
      prefix = 'Шаги';
      break;
    case 'warn':
      prefix = 'Предупреждение';
      break;
    case 'debug':
      prefix = 'Отладка';
      break;
    case 'error':
      prefix = 'Ошибка';
      break;
    case 'info':
      prefix = 'Инфо';
      break;
  }
  
  return console[type].bind(console, `[${ prefix }]`);
}

export const log = {
  /**
   * Краткие, лаконичные сообщения
   * @param message
   */
  step: makeLogger('log'),
  
  /**
   * Сообщения о неожиданном или опасном поведении приложения
   * @param message
   */
  warn: makeLogger('warn'),
  
  /**
   * Сообщения об ошибках и исключениях
   * @param message
   */
  error: makeLogger('error'),
  
  /**
   * Отладочные сообщения
   * @param message
   */
  debug: makeLogger('debug'),
  
  /**
   * Максимально подробные сообщения
   * @param message
   */
  info: makeLogger('info')
}
