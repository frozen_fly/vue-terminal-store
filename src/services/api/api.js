import defaultRequest from "@/services/DefaultAxiosInstance";
import { log } from "@/helpers/log";
import settings from "@/store/settings";
import { catalogMock } from "@/helpers/mock/goodsMock";

/**
 * Запрос информации по товару
 * @param {string} barcode
 * @return {Promise<Object|null>}
 */
const requestProduct = barcode => {
  if (settings.state.debug.mock) {
    log.debug('Добавляем случайный товар из моков')
    const randomNum = Math.floor(Math.random()*catalogMock.length)
    return Promise.resolve(catalogMock[randomNum])
  }
  
  return defaultRequest.post(`/finditem?barcode=${barcode}`)
    .then(response => {
      try {
        const productInfo = JSON.parse(response.data)
        log.debug(`[Товар] Код ответа сервера: ${response.status}`);
        log.info('[Товар] Получили ответ от сервера: ', response.data);
        if (typeof productInfo === 'object') {
          return productInfo;
        } else {
          log.error('[Товар] Получили не объект')
          return null
        }
      } catch (e) {
        log.error(`[Товар] Не удалось распаковать данные: ${JSON.stringify(response, null, 2)}, ошибка: `, e)
        return null
      }
    })
    .catch(e => {
      log.error('[Товар] Не удалось получить данные. Ошибка: ', e)
      return null
    });
}

/**
 * Запрос на оплату
 * @param {object} data
 * @return {Promise<Boolean>}
 */
const requestSetPay = data => {
  try {
    if (settings.state.debug.on) {
      log.info('Режим отладки, не отправляем оплату')
    } else {
      log.info('[Оплата] Отправляем запрос на подтверждение оплаты')
      log.debug('[Оплата] Данные по оплате: ', data)
      
      return defaultRequest.post('/setPay/Pay', data)
        .then(response => {
          log.debug(`[Оплата] Код ответа сервера: ${response.status}`)
          log.info('[Оплата] Получили ответ от сервера: ', response.data)
          
          if (response.data.ResultState) {
            log.info('[Оплата] Данные по оплате успешно отправлены, очередь не создаем')
            return true
          } else {
            log.error('[Оплата] Ошибка при передаче данных по оплате')
            return false
          }
        })
        .catch(e => {
          log.error('[Оплата] Ошибка при передаче данных по оплате. Ошибка: ', e)
          return false
        });
    }
  } catch (e) {
    log.error(e)
    log.error('Ошибка при передаче данных по оплате! Оплата не передана!')
  }
}

export {
  requestProduct,
  requestSetPay
}
