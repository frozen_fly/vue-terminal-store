import Vue from 'vue'
import App from './App.vue'
import store from './store/store'

import PortalVue from 'portal-vue';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import 'normalize.css';

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.directive('visible', (el, { value }) => {
  el.style.visibility = value ? 'visible' : 'hidden';
});

Vue.config.productionTip = false;

Vue.use(PortalVue);

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
