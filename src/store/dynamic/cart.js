import { log } from "@/helpers/log";
import {roundNumber} from "@/helpers/utils";

const state = () => ({
  list: {},
});

const getters = {
  size: state => Object.keys(state.list).length,
  fullSize: state => Object.values(state.list).reduce((acc, el) => { return acc + el.quantity }, 0),
  sum: state => Object.values(state.list).reduce((acc, el) => roundNumber(acc + el.sum), 0),
};

const actions = {
  add({ commit }, obj) {
    if (isDataValid(obj)) {
      commit('ADD', obj);
    } else {
      log.warn(`На добавление в корзину передан невалидный объект ${JSON.stringify(obj)}`);
    }
  },
  remove({ commit }, {id, quantity = 1}) {
    const params = {
      id: id,
      quantity: quantity,
    }
    
    commit('REMOVE', params);
  },
  purge({ commit }) {
    log.step('Очищаем корзину');
    commit('PURGE');
  }
};

const mutations = {
  ADD(state, obj) {
    const { id, quantity } = obj;
    
    if (id in state.list) {
      state.list[id].quantity += quantity;
      state.list[id].sum = roundNumber(state.list[id].price * state.list[id].quantity);
      log.step(`Увеличиваем количество позиции #${id} ${obj.name} на ${quantity}`);
    } else {
      state.list = {
        ...state.list,
        [id]: obj,
      };
      log.step(`Добавляем в корзину позицию #${id} ${obj.name}`);
    }
  },
  REMOVE(state, { id, quantity }) {
    if (id in state.list) {
      console.log(quantity)
      if (quantity >= state.list[id].quantity || quantity === 0) {
        log.step(`Удаляем из корзины позицию #${id} ${state.list[id].name}`);
        state.list = Object.fromEntries(Object.entries(state.list).filter(pair => pair[0] !== id));
      } else {
        log.step(`Уменьшаем количество позиций #${id} ${state.list[id].name} в корзине`);
        state.list[id].quantity -= quantity;
        state.list[id].sum = roundNumber(state.list[id].price * state.list[id].quantity)
      }
    } else {
      log.warn(`В корзине отсутствует элемент с id ${id}`);
    }
  },
  PURGE(state) {
    state.list = {};
  },
};

const isDataValid = obj =>
  typeof obj === 'object'
  && obj !== null
  && ['id', 'name', 'price', 'quantity'].every(param => param in obj)
  && (typeof obj.id === "string" || typeof obj.id === "number")
  && typeof obj.name === 'string'
  && typeof obj.price === "number"
  && typeof obj.quantity === "number";

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
