import { log } from "@/helpers/log";
import Vue from 'vue';

const state = () => ({
  is: false,
  info: null,
  success: false,
  type: null,
  finished: false,
  sum: {
    input: 0,
    total: 0,
  },
  polling: {
    timer: null,
    delay: 200,
  },
  pinpadDone: null,
  goodsList: null,
  infinite: false,
  combo: null,
  autoFinishCalled: false,
  cashConfirmDelay: 9 * 1000,
});

const getters = {
  remainingSum: state => {
    const sum = state.sum.total - state.sum.input;
    return sum < 0 ? 0 : sum;
  },
  changeSum: state => {
    const change = state.sum.total - state.sum.input;
    return change < 0 ? Math.abs(change) : 0;
  },
  receivedSum: state => {
    if (state.type === 'cash') {
      if (isNaN(Number(state.sum.input)) || isNaN(Number(state.sum.total))) {
        return 0;
      }
      
      if (state.sum.input < state.sum.total) {
        return state.sum.input;
      } else {
        return state.sum.total;
      }
    }
    
    if (state.type === 'card') {
      return state.sum.total;
    }
    
    return 0;
  },
};

const actions = {
  async start({ state, commit, dispatch, rootState }, { goodsList, type = state.type, sum, }) {
    return new Promise((resolve, reject) => {
      log.step('Подготавливаем платёж');
      
      if (goodsList === undefined) {
        log.error('Платёж не может быть начат - отсутствует GoodsList');
        reject();
        return;
      } else {
        dispatch('setGoodsList', goodsList);
      }
      
      try {
        dispatch('setType', type);
      } catch (e) {
        log.error('Платёж не может быть начат - не задан тип платежа');
        reject();
        return;
      }
      
      if (state.infinite && state.goodsList.length > 1) {
        log.error('Номенклатура не может состоять из более чем одной позиции при неограниченной оплате наличными');
        reject();
        return;
      }
      
      let sumToPay;
      if (state.infinite && state.type === 'cash') {
        sumToPay = 0;
      } else {
        sumToPay = sum;
        
        if (sumToPay === 0) {
          if (state.type === 'card') reject();
          
          log.error('Сумма оплаты на ' + (state.type === 'card' ? 'без' : '') + 'нале ' +
            'не может быть равна нулю' +
            (state.type === 'cash' ? '. Возможно вы хотели воспользоваться методом enableInfinite' : ''));
          reject();
          return;
        }
      }
      if (sumToPay !== 0 && sumToPay !== Number(sumToPay.toFixed(2))) {
        log.error(`Неверная сумма ${sumToPay}. Нужно округлить до второго знака после запятой`);
        reject();
        return;
      }
      
      commit('SET_PAYMENT_SUM', sumToPay);
      
      commit('UPDATE_PAYMENT_STATUS', true);
      let stopCardWatcher, timeoutSeconds, decreaseSeconds;
      let prevInput = 0;
      
      switch (state.type) {
        case 'cash':
          commit('SET_POLLING_TIMER',
            setInterval(function () {
              const inputSum = state.sum.input;
              
              if (prevInput !== inputSum) {
                prevInput = inputSum;
                commit('UPDATE_INPUT_SUM', inputSum)
                const totalSum = state.sum.total;
                
                if (rootState.settings.payment.autoFinish
                  && inputSum >= totalSum
                  && !state.infinite
                ) {
                  clearInterval(state.polling.timer);
                  dispatch('confirmCash');
                }
              }
            }, state.polling.delay)
          );
          break;
        case 'card':
          timeoutSeconds = Vue.observable({
            value: 120,
          });
          decreaseSeconds = setInterval(() => {
            timeoutSeconds.value--
            if (timeoutSeconds.value <= 0) {
              clearInterval(decreaseSeconds);
            }
          }, 1000);
          
          stopCardWatcher = this.watch(
            () => {
              return state.success
                || state.pinpadDone
                || timeoutSeconds.value <= 0;
            },
            () => {
              stopCardWatcher();
              
              if (timeoutSeconds.value <= 0) {
                log.warn('Интерфейсный таймаут пинпада');
                commit('UPDATE_PAYMENT_STATUS', false);
                reject();
                return;
              }
              
              if (state.success) {
                dispatch('sendGoodsList');
                resolve();
              } else {
                commit('UPDATE_PINPAD_STATUS', false);
                commit('UPDATE_PAYMENT_STATUS', false);
                reject();
              }
            }
          );

          break;
        default:
          log.error(`Не задан валидный тип оплаты. Текущий тип: ${state.type}`);
      }
      
      log.step('Платёж подготовлен');
    });
  },
  
  async interrupt({ state, commit }, reason) {
    log.step(`Прерываем оплату по причине: ${reason}`)
    commit('UPDATE_PINPAD_STATUS', true);
  },
  
  /**
   * Вызов успешного платежа с наличной оплаты
   * @returns {Promise}
   * resolve - в случае передачи управления на бэк с уходом на ShowPrintCheckPage
   * reject - в случае полного отказа от проведения. Управление сразу передаётся интерфейсу
   */
  confirmCash({ state, commit, dispatch, rootState }) {
    log.info('Вызов успешного платежа с наличной оплаты в хранилище');
    
    return new Promise((resolve, reject) => {
      if (state.is === true) {
        if (state.type !== 'cash') {
          log.error('Попытка вызова успешного завершения приёма купюр вне наличной оплаты');
          reject();
          return;
        }
        
        log.step('Вызван обработчик успешного выполнения наличной оплаты');
        
        setTimeout(() => {
          commit('UPDATE_PAYMENT_STATUS', false);
        
          clearInterval(state.polling.timer);
          
          if (
            rootState.settings.payment.allowPartial.cash
            ? state.sum.input > 0
            : state.sum.input >= state.sum.total
          ) {
            commit('SET_PAYMENT_SUCCESS');
            dispatch('sendGoodsList');
            setTimeout(window.ShowPrintCheckPage);
            
            resolve();
            return;
          } else {
            if (state.sum.input === 0) {
              log.error('Отсутствуют принятые деньги');
              reject();
              return;
            } else {
              log.warn('Отказ проведения платежа, т.к. введённая сумма недостаточна');
              setTimeout(window.ShowPrintCheckPage);
              resolve();
              return;
            }
          }
        }, state.cashConfirmDelay);
      } else {
        log.error('Попытка вызвать успешное завершение платежа, когда оплата неактивна');
        reject();
        return;
      }
    });
  },
  
  /**
   * Вызов отмены платежа с наличной оплаты
   * @returns {Promise}
   * resolve - в случае передачи управления на бэк с уходом на ShowPrintCheckPage
   * reject - в случае полного отказа от проведения. Управление сразу передаётся интерфейсу
   */
  cancelCash({ state, commit, dispatch, rootState }) {
    log.info('Вызов отмены платежа с наличной оплаты в хранилище');
    
    return new Promise(resolve => {
      if (state.is === true) {
        if (state.type !== 'cash') {
          log.error('Попытка вызова отмены завершения приёма купюр вне наличной оплаты');
          resolve(true);
          return;
        }
        
        log.step('Вызван обработчик отмены выполнения наличной оплаты');
        
        setTimeout(() => {
          commit('UPDATE_PAYMENT_STATUS', false);
          
          clearInterval(state.polling.timer);
          
          if (state.sum.input === 0) {
            resolve(true);
            return;
          } else {
            log.warn('При отмене присутствовали деньги');
            
            if (state.sum.input >= state.sum.total) {
              log.step('Денег достаточно, проводим оплату');
              commit('SET_PAYMENT_SUCCESS');
              dispatch('sendGoodsList');
              setTimeout(window.ShowPrintCheckPage);
              
              resolve(false);
              return;
            } else {
              log.step('Денег недостаточно, возвращаем деньги');
              setTimeout(window.ShowPrintCheckPage);
              
              resolve(false);
              return;
            }
          }
        }, state.cashConfirmDelay);
      } else {
        log.error('Попытка вызвать отмену платежа, когда оплата неактивна');
        resolve(true);
        return;
      }
    });
  },
  
  /**
   * Обработка успешной оплаты по банковскому модулю
   * @param state vuex
   * @param commit vuex
   */
  handleCardSuccess({ state, commit }) {
    log.step('Получен успех от банковского модуля');
    
    commit('SET_CARD_SUCCESS');
  },
  
  /**
   * Завершить платёж
   */
  finalize({commit}) {
    commit('UPDATE_PAYMENT_STATUS', false);
    setTimeout(() => {
      log.step('Проведение оплаты завершено');
      commit('SET_PAYMENT_FINISHED');
    }, 4 * 1000);// 4 секунды - минимальное время для бэка, чтобы отправить платёж в личный кабинет
  },
  
  setType({commit}, type) {
    commit('SET_TYPE', type);
  },
  
  /**
   *
   * @param state
   * @param commit
   * @param goodsList{Array<GoodsListItem>|GoodsListItem}
   */
  setGoodsList({commit}, goodsList) {
    if (!Array.isArray(goodsList)) {
      goodsList = [goodsList];
    }
    commit('SET_GOODSLIST', goodsList);
  },
  
  sendGoodsList({ state, commit }) {
    if (state.infinite && state.type === 'cash') {
      commit('UPDATE_GOODSLIST_INFINITE');
    }
  }
};

const mutations = {
  SET_TYPE(state, type) {
    state.type = type;
  },
  SET_CARD_SUCCESS(state) {
    state.success = true;
    state.pinpadDone = true;
  },
  SET_PAYMENT_SUCCESS(state) {
    state.success = true;
  },
  SET_PAYMENT_FINISHED(state) {
    state.finished = true;
  },
  SET_PAYMENT_SUM(state, sum) {
    state.sum.total = sum;
  },
  SET_PAYMENT_INFO(state, info) {
    state.info = info;
  },
  SET_GOODSLIST(state, goodsList) {
    state.goodsList = goodsList;
  },
  SET_POLLING_TIMER(state, timer) {
    state.polling.timer = timer;
  },
  UPDATE_PAYMENT_STATUS(state, status) {
    state.is = Boolean(status);
  },
  UPDATE_PINPAD_STATUS(state, status) {
    state.pinpadDone = Boolean(status);
  },
  UPDATE_INPUT_SUM(state, sum) {
    state.sum.input = sum;
  },
  UPDATE_GOODSLIST_INFINITE(state) {
    state.goodsList[0].Price = state.sum.input;
  },
  DEBUG_ADD_INPUT_SUM(state, sum) {
    state.sum.input += sum;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
