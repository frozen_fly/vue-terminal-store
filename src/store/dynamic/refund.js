import { log } from "@/helpers/log";
import { getLastPayment } from "@/helpers/utils";
import moment from 'moment';

const state = () => ({
  done: false,
  type: null,
  info: null,
  sum: {
    actual: null,
    required: null,
  },
  timers: {
    refund: undefined,
  },
  response: null,
  readyRequestRunning: false,
});

const getters = {};

const actions = {
  execute({ state, rootState }, { request, stopGlobalTimer, startGlobalTimer }) {
    return new Promise(((resolve, reject) => {
      if (
        typeof request !== "object"
        || typeof stopGlobalTimer !== "function"
        || typeof startGlobalTimer !== "function"
      ) {
        log.error('Не переданы все параметры для старта возврата');
        reject();
        return;
      }
      
      let linkNum, authCode;
      if (request.type === 'card') {
        linkNum = request.card?.linkNum ?? 0;
        authCode = request.card?.authCode ?? 0;
      } else {
        linkNum = 0;
        authCode = 0;
      }
      
      stopGlobalTimer();
      
      const startTime = moment();
      const startTimeString = startTime.format('DD.MM.YYYY HH:mm:ss');
      let lastNotificationTime = startTime;
      const timer = setInterval(() => {
        const timeNow = moment();
        
        if (state.done) {
          clearInterval(timer);
          startGlobalTimer();
          resolve();
        } else {
          if (Math.ceil(moment.duration(startTime.diff(timeNow)).as('minutes')) * -1 > 30) {
            if (moment.duration(lastNotificationTime.diff(timeNow)).as('minutes') * -1 > 10) {
              lastNotificationTime = timeNow;
              log.warn(`Выдача средств на возврате (${startTimeString}) производится слишком долго`);
            }
          }
        }
      }, 100);
      // не предполагается, что при штатной работе бэк не вызовет обработчики возврата, поэтому тайм-аута нет
    }));
  },
  
  finalize: async ({ commit, rootState }, data) => {
    commit('SET_FINISHED_REFUND_DATA', data);
  }
};

const mutations = {
  SET_REFUND_INFO(state, info) {
    state.info = info;
  },
  SET_FINISHED_REFUND_DATA(state, { sum, type }) {
    state.sum = sum;
    state.type = type;
    state.done = true;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
