const state = {
  debug: {
    on: 1,
    hotkeys: 0,
    layout: 0,
    borders: 0,
    setSteps: 0,
    alert: 0,
    mock: 1,
    devIntegration: 1,
    integration: { // отладочные настройки интеграции
      A: 'https://91.205.191.122/EEP/hs/find',
      B: '2050',
      C: '',
      D: '',
      E: '',
      F: '',
    },
  },
  schedule: false,
  log: {
    on: 1,
    step: 1,
    warning: 1,
    exception: 1,
    debug: 1,
  },
  gateId: 2050,
  timeout: 60 * 1000,
  clock: true,
  payment: {
    allowChange: false,
    allowPartial: {
      cash: false,
      card: false,
    },
    autoFinish: false,
    getAllInfo: true,
    combo: false,
    defaults: {
      taxGroup: 'default',
      typeSign: 'default',
      itemSign: 'default',
    },
    online: {
      settings: {
        auth: {
          companyId: 0,
          userLogin: '',
          password: '',
        },
        data: {
          getFiscalData: true,
          getCheckLink: false,
          getQRCode: false,
        },
        database: {
          username: '',
          password: '',
          database: '',
        },
        check: {
          taxSystemType: 1,
        }
      }
    }
  },
  refund: {
    getAllInfo: false,
    defaults: {
      taxGroup: 1,
      typeSign: 4,
      itemSign: 1,
    },
  },
  cashbox: {
    debug: false,
    dbNumber: '0',
  }
};

const getters = {
  mockEnabled: state => state.debug.on && state.debug.mock,
  isDebug: state => Boolean(state.debug.on),
};

export default {
  namespaced: true,
  state,
  getters
};
