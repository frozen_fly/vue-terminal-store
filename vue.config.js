module.exports = {
  publicPath: '',
  lintOnSave: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/assets/styles/variables.scss";'
      }
    }
  }
}
